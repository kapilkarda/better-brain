app.controller('LoginCtrl', function LoginCtrl($scope, $http, $localstorage) {
	'use strict';

	var isLogin = $localstorage.get("isLogin");
	if(isLogin=="1"){
		window.location.href = "#/home";
	}

	$scope.hideContent = "hide";
	$scope.hideContentMain = "";

	$scope.formData = {};
	$scope.formData.email = "";
	$scope.formData.password = "";

	$scope.doLogin = function(){
		if($scope.formData.email==""){
			$scope.hideContent = "";
			$scope.hideContentMain = "Email is not blank";
			return false;
		}
		if($scope.formData.password==""){
			$scope.hideContent = "";
			$scope.hideContentMain = "Password is not blank";
			return false;
		}
		$scope.hideContent = "hide";
		$scope.hideContentMain = "";

		$http.get("adminlogin/"+$scope.formData.email+"/"+$scope.formData.password)
	    .success(function(response) {
	    	if(response.result){
	    		$localstorage.set("isLogin","1");
	    		window.location.href = "#/home";
	    	}else{
	    		$scope.hideContent = "";
				$scope.hideContentMain = "Invalide Credential";
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}
});

app.controller('HomeCtrl', function LoginCtrl($scope, $http) {
	'use strict';
	$scope.questionCount = "0";
	$scope.userCount = "0";

	$scope.getUserData = function(){
		$http.get("userlist")
	    .success(function(response) {
	    	if(response.result){
	    		$scope.userCount = response.data.length;
	    		$scope.getQuestionData();
	    	}else{
	    		$scope.users = [];
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}

	$scope.getQuestionData = function(){
		$http.get("questions")
	    .success(function(response) {
	    	if(response.result){
	    		$scope.questionCount = response.data.length;
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}

	$scope.getUserData();

});

app.controller('HeaderCtrl', function LoginCtrl($scope, $http, $localstorage) {
	'use strict';
	$scope.doLogout = function(){
		$localstorage.set("isLogin", "0");
		window.location.href = "#/";
	}
});

app.controller('UserCtrl', function LoginCtrl($scope, $http) {
	'use strict';
	
	$scope.users = [];

	$scope.getUserData = function(){
		$http.get("userlist")
	    .success(function(response) {
	    	if(response.result){
	    		$scope.users = response.data
	    	}else{
	    		$scope.users = [];
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}
	$scope.getUserData();


	$scope.removeUser = function(userId){
		$http.get("removeUser/"+userId)
	    .success(function(response) {
	    	if(response.error){
	    		alert(response.message);
	    	}else{
	    		alert(response.message);
	    		$scope.getUserData();
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}

	$scope.loadUserQuestion = function(userId){
		$http.get("getScores/"+userId)
	    .success(function(response) {
	    	$("#myModal").modal("show");
	    	if(response.result){
	    		$scope.scores = response.data;
	    	}else{
	    		$scope.scores = [];
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}

});

app.controller('QuestionCtrl', function LoginCtrl($scope, $http) {
	'use strict';

	$scope.cardArray = [
		{'name':'1c'}, {'name':'2c'}, {'name':'3c'}, {'name':'4c'}, {'name':'5c'}, {'name':'6c'}, {'name':'7c'}, {'name':'8c'}, {'name':'9c'}, {'name':'Tc'}, {'name':'Jc'}, {'name':'Qc'}, {'name':'Kc'}, {'name':'1h'}, {'name':'2h'}, {'name':'3h'},{'name':'4h'},{'name':'5h'},{'name':'6h'},{'name':'7h'},{'name':'8h'},{'name':'9h'},{'name':'Th'},{'name':'Jh'},{'name':'Qh'},{'name':'Kh'},{'name':'1s'},{'name':'2s'},{'name':'3s'},{'name':'4s'},{'name':'5s'},{'name':'6s'},{'name':'7s'},{'name':'8s'},{'name':'9s'},{'name':'Ts'},{'name':'Js'},{'name':'Qs'},{'name':'Ks'},{'name':'1d'},{'name':'2d'},{'name':'3d'},{'name':'4d'},{'name':'5d'},{'name':'6d'},{'name':'7d'},{'name':'8d'},{'name':'9d'},{'name':'Td'},{'name':'Jd'},{'name':'Qd'},{'name':'Kd'}
	];
	$scope.position = [];
	$scope.questiontype = [];
	$scope.questionTopic = [];
	$scope.questions = [];

	$scope.editForm = {
		card1 		: '',
		card2 		: '',
		position 	: '',
		type 		: '',
		topic 		: '',
		question	: 'Do you push or fold?',
		opt1 		: 'Push',
		opt2 		: 'Fold',
		opt3 		: '',
		opt4 		: '',
		right_opt 	: '',
		cat 		: '',
		sub_cat 	: '',
		level 		: '',
		game_type 	: '',
		stacksize 	: '',
		bb 			: '',
		sb 			: '',
		ante	 	: ''
	};

	$scope.position = [
		{position:'1', name:'1(button)'},
		{position:'2', name:'2(sb)'},
		{position:'3', name:'3(bb)'},
		{position:'4', name:'4(under the gun)'},
		{position:'5', name:'5'},
		{position:'6', name:'6'},
		{position:'7', name:'7'},
		{position:'8', name:'8'},
		{position:'9', name:'9(cut off)'},
	];
	
	$scope.loadQuestionType = function(){
		$http.get("question_type")
	    .success(function(response) {
	    	if(response.result){
	    		$scope.questiontype = response.data
	    		$scope.editForm.type = $scope.questiontype[0]._id;
	    		$scope.editForm.position = $scope.position[0].position;
	    		$scope.editForm.card1 = $scope.cardArray[0].name;
	    		$scope.editForm.card2 = $scope.cardArray[0].name;
	    		$scope.loadTopics();
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}

	$scope.loadTopics = function(){
		$http.get("topics")
	    .success(function(response) {
	    	if(response.result){
	    		$scope.questionTopic = response.data
	    		$scope.editForm.topic = $scope.questionTopic[0]._id;
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}

	$scope.getQuestionData = function(){
		$http.get("questions")
	    .success(function(response) {
	    	if(response.result){
	    		$scope.questions = response.data;
	    		$scope.loadQuestionType();
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}

	$scope.getQuestionData();


	$scope.saveEdit = function(){

		var dataset = {"question":$scope.editForm.question, "type_id":$scope.editForm.type, "topic_id":$scope.editForm.topic, "opt1":$scope.editForm.opt1, "opt2":$scope.editForm.opt2, "opt3":$scope.editForm.opt3, "opt4":$scope.editForm.opt4, "right_answer":$scope.editForm.right_opt, "category":$scope.editForm.cat, "sub_category":$scope.editForm.sub_cat, "lavel":$scope.editForm.level, "type":$scope.editForm.game_type, "stacksizebb": $scope.editForm.stacksize, "bb":$scope.editForm.bb, "sb":$scope.editForm.sb, "ante":$scope.editForm.ante, "card1":$scope.editForm.card1, "card2":$scope.editForm.card2, "position":$scope.editForm.position };

		console.log(dataset);

		var settings = {
		  "url": "setQuestion",
		  "method": "POST",
		  "headers": {
		    "content-type": "application/x-www-form-urlencoded"
		  },
		  "data": dataset
		}

		$.ajax(settings).done(function (response) {
		  	if(response.result){
	    		$scope.editForm = {
					card1 		: '',
					card2 		: '',
					position 	: '',
					type 		: '',
					topic 		: '',
					question	: 'Do you push or fold?',
					opt1 		: 'Push',
					opt2 		: 'Fold',
					opt3 		: '',
					opt4 		: '',
					right_opt 	: '',
					cat 		: '',
					sub_cat 	: '',
					level 		: '',
					game_type 	: '',
					stacksize 	: '',
					bb 			: '',
					sb 			: '',
					ante	 	: ''
				};
				$scope.getQuestionData();
				alert("Successfully Added");
				$("#myModal").modal("hide");
				$(".modal-backdrop").remove();
	    	}
		});
	}

	$scope.removeQuestion = function(question_id){
		$http.get("removeQuestion/"+question_id)
	    .success(function(response) {
	    	if(response.error){
	    		alert(response.message);
	    	}else{
	    		alert(response.message);
	    		$scope.getQuestionData();
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}

});

app.controller('ChangePasswordCtrl', function LoginCtrl($scope, $http) {
	'use strict';
});

app.controller('TopicCtrl', function LoginCtrl($scope, $http) {
	'use strict';
	$scope.topics = [];

	$scope.getTopicData = function(){
		$http.get("topics")
	    .success(function(response) {
	    	console.log(response);
	    	if(response.result){
	    		$scope.topics = response.data
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}
	$scope.getTopicData();
});

app.controller('QuestionTypeCtrl', function LoginCtrl($scope, $http) {
	'use strict';
	$scope.types = [];
	$scope.getTypeData = function(){
		$http.get("question_type")
	    .success(function(response) {
	    	console.log(response);
	    	if(response.result){
	    		$scope.types = response.data
	    	}
	    }).error(function(data) {
	    	console.log(data);
	    });
	}
	$scope.getTypeData();
});
