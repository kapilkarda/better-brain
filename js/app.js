var app = angular.module('betterbrain', ['ngRoute'])

app.run(function($rootScope){
	$rootScope.dataPrintRunRoot = "CHECKING";
});

app.config(function ($routeProvider) {
	'use strict';

	var otherwiseURL = "/";
	var isLogin = localStorage.getItem("isLogin");
	if(isLogin=="1"){
		otherwiseURL = "/home";
	}

	$routeProvider
		.when('/', {
			controller: 'LoginCtrl',
			templateUrl: 'templates/login.html'
		})
		.when('/home', {
			controller: 'HomeCtrl',
			templateUrl: 'templates/home.html'
		})
		.when('/user', {
			controller: 'UserCtrl',
			templateUrl: 'templates/user.html'
		})
		.when('/question', {
			controller: 'QuestionCtrl',
			templateUrl: 'templates/question.html'
		})
		.when('/changepassword', {
			controller: 'ChangePasswordCtrl',
			templateUrl: 'templates/changepassword.html'
		})
		.when('/topics', {
			controller: 'TopicCtrl',
			templateUrl: 'templates/topics.html'
		})
		.when('/questiontype', {
			controller: 'QuestionTypeCtrl',
			templateUrl: 'templates/question_type.html'
		})
		.otherwise({
			redirectTo: otherwiseURL
		});
});

app.factory('$localstorage', ['$window', function($window) {
    return {
	    set: function(key, value) {
	     $window.localStorage[key] = value;
	    },
	    get: function(key, defaultValue) {
	     return $window.localStorage[key] || defaultValue;
	    },
	    setObject: function(key, value) {
	     $window.localStorage[key] = JSON.stringify(value);
	    },
	    getObject: function(key) {
	     return JSON.parse($window.localStorage[key] || '{}');
	    }
    }
}]);
