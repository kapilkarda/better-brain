//random = require('mongoose-simple-random'),

var express  = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    sha1 = require('sha1'),
    fs = require('fs'),
    random = require('mongoose-simple-random'),
    cloudinary = require('cloudinary'),

    AdminSchema = new mongoose.Schema({
      email     : { type: String, required: true, unique: true },
      pwd       : { type: String, required: true }
    }),

    Admin = mongoose.model('Admin', AdminSchema);

    UserSchema = new mongoose.Schema({
      fullname  : String,
      username  : String,
      email     : { type: String, required: true, unique: true },
      pwd       : { type: String, required: true },
      gender    : String,
      fb_id     : String,
      fb_token  : String,
      avator    : String,
      country   : String,
      dob       : String,
      userstatus: Boolean
    }),
    User = mongoose.model('User', UserSchema);

    Useravgpoint = new mongoose.Schema({
      user_id           : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      question_attempt  : String,
      right_question    : String,
      wrong_question    : String,
      total_point       : Number
    }),
    Userpoint = mongoose.model('Userpoint', Useravgpoint);

    questionSchema = new mongoose.Schema({
      question      : String,
      type_id       : String,
      topic_id      : String,
      opt1          : String,
      opt2          : String,
      opt3          : String,
      opt4          : String,
      right_answer  : String,
      category      : String,
      sub_category  : String,
      level         : String,
      type          : String,
      stacksizebb   : String,
      bb            : String,
      sb            : String,
      ante          : String,
      card1         : String,
      card2         : String,
      position      : String     
    }),

    questionSchema.plugin(random);
    //questionSchema.plugin(random, { path: 'r' });
    //questionSchema.plugin(random, {});

    Question = mongoose.model('Question', questionSchema);

    topicSchema = new mongoose.Schema({
      title              : String,
      icon               : String,
      question_coun      : Number
    }),

    Topic = mongoose.model('Topic', topicSchema);

    questionTypeSchema = new mongoose.Schema({
      title              : String,
      question_coun      : Number
    }),

    QuestionType = mongoose.model('QuestionType', questionTypeSchema);

    insertScoreSchema = new mongoose.Schema({
        topic_id        : String,
        user_id         : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        question_id     : {type: mongoose.Schema.Types.ObjectId, ref: 'Question' },
        answer          : String,
        point           : String,
        isTrue          : Boolean,
        isFalse         : Boolean
    }),

    InsertScore = mongoose.model('InsertScore', insertScoreSchema);

var GoogleSpreadsheet = require("google-spreadsheet");

var GoogleSpreadsheets = require('google-spreadsheets');
var google = require('googleapis');

var multer     =       require('multer');

var uristring =
process.env.MONGOLAB_URI ||
process.env.MONGOHQ_URL ||
'mongodb://localhost:27017/betterbrain1';

var creds = {
  client_email: 'eserviss-1069@appspot.gserviceaccount.com',
  private_key: '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCNIa8oHFQdE8y2\ni9E8T2qqWxqIeG0LxQUaOetvgKeiyxxkjKeLmjyiZ6ENTH/WyQyTZAMOA/OzzLYX\novIMfNtERS/H3Mwwy/2vjK8VbeYOveXYkxDifyt4Xdcco0VjoPSLUBmWLgUg4viL\nhlxoDk8XjwMrO4gwjQNQxseGhU9ctc8JXLgXgtYGgYBxdE8mUdgvjLVt2r/wh9C/\nOFw1p94O/caGISCnktR3dJ48832ZGbo8ntWTTvbyyZ+BBlJjBWjRxVjLLjlOsUEY\nF2b1KjyByvSz5i2uVN+HcIhxoLjTZPqXeufEi8hiUIazWYrzqFaSDEXIKRzQbAk6\nT7TbjyGZAgMBAAECggEBAIoNWBQJWfIziv1J2XuAR9uqUFMMUcbAi5Qy8e1ZUSG6\nQuTMfZveXM+URQMIQMjuU2Hd4+sSzJGlfr2yQ8kZXTbz4qBDiG13LYlKOX3hybfD\n5BCtE0grk28jhCdcQqUJLg0UZmL0U0nvG6cH4F5dGNr3pubkTf0IAyVBvK0GvDH3\nmugRQJQlphkzvE3k9lvZrD/9eZV3BIDqbn8OnDnbt8dMkJBrvJIkxywYccpxuB6t\nfkTzw2IZMarc8b2h1zap7eJvvpFqTSNY+vC65nRaacA43tkkGzhgmw0+S3BpOx67\nIPWpV51ATfXqNOkM063mzZS1zaujDT1+Q0NTSv6WwbECgYEA82tEUovN/DIKNxcu\nrxuWuyLSi13138x9s1MoibZboVB06JljHVhG6eTAtLrVwlBdY2uTJEIs/xkhwgK5\nCleTMjovD/y1Te9s6E9N0mhzYguRrg/TiKOTuqauWAHBJ7pKNEZTvOrV+AQDOwCp\nyI7td+TmkpFOdb9/rWcGb2tTIlUCgYEAlG0HWLm+nA7NFMEI/Evix1zBJ4Gwe7eJ\nhCsbIVXoP/mM894+yideshWBcOPvPz3uamXnmHyralGQlvcIoPpewGiD5iy/S6ka\nYkH1HWA6qyeET2oIt0HzbSAy+EzPKwdjo1owTZHsLJjoQ+sAX6fYBOgJB8yRpKXh\nVo1C1S227jUCgYAnvBjZCK1FP73fJE+gkfZW8eZAjcP3FmBmh2qUoEnw2TtcRL2j\nKqfYwliap+0A3TGBaywHvS2vLqEOFHe0Mnt6tXi9OVk3MaArg9aGRDqhhJXxPfAf\n2+Pfkhnfj9nB7Gz0f45nxkXLSIxmc0ND0+D7s0fYIRQXsHzdB3ZLdc+KSQKBgBFC\ngHkz2FODcRu5utbD6FIwmhGDBkfMtwQUkyoUOFbAKUDuljSh0WQ6FfxPtTGQfRoI\nUZ8s2C4b4Al+HpXBp6UOQlBQ4cnllzjX3K4W05u1k7A8b1kIJUuKVAMFw42IZXDI\nexd6IZGqElBbkoaNGRSw+uPaSRFxbLvI3320vDIFAoGBAJ5DgFw91jPqVddf5rFl\nQ7ZhH/EyJYdqjqDfPqy3IglrJ4iNHbYDwE2aQQG2oltfVg2IHKB3aV+EgT6qv6K+\noI3dr5myZrs+HxlQG6yGX+WaDl5oa5EwsYZmCN3l8zEYiCjCwrBcdw+M9H1YBZJ8\nKNJzKFwSudzD1zpcc1suKXuY\n-----END PRIVATE KEY-----\n'
}

// var uristring =
// process.env.MONGOLAB_URI ||
// process.env.MONGOHQ_URL ||
// 'mongodb://betterbrain:betterbrain123@ds049104.mongolab.com:49104/heroku_9rrp9jz8';

var upload      =     multer({ dest: 'uploadimage/'});

var db;

mongoose.connect(uristring, function (err, res) {
  if (err) {
    console.log ('ERROR connecting to: ' + uristring + '. ' + err);
  } else {
    console.log ('Succeeded connected to: ' + uristring);
    db = res;
  }
});

cloudinary.config({ 
  cloud_name: 'hxh7865ld', 
  api_key: '177311831149685', 
  api_secret: 'l8ylD8jcNSdqD1SAcYPrvl4wPVk' 
});

express()
  .use(bodyParser.json()) // support json encoded bodies
  .use(bodyParser.urlencoded({ extended: true })) // support encoded bodies
  .use(multer({ dest: 'uploadimage/',
    rename: function (fieldname, filename) {
      return filename+Date.now();
    },
    onFileUploadStart: function (file) {
      //console.log(file.originalname + ' is starting ...');
    },
    onFileUploadComplete: function (file) {
      //console.log(file.fieldname + ' uploaded to  ' + file.path)
    }
  }))
  .get('/adminlogin/:email/:pwd', function(req, res) {
    //var db = req.db;
    //var collection = db.get('users');
    var email = req.params.email;
    var pwd = sha1(req.params.pwd); //req.params.pwd;
    Admin.find({'email':email, 'pwd':pwd},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .get('/addAdmin/:email/:pwd', function(req, res) {
    var email = req.params.email;
    var pwd = sha1(req.params.pwd);

    Admin.find({'email':email},{},function(e,docs){
      if(docs.length==0){
        var user = new Admin({'email':email, 'pwd':pwd});
        user.id = user._id;
        user.save(function (err) {
          //res.json(200, todo);
          res.send(
                (err === null) ? { result: true, data: user  } : {result: false, msg: err }
            );
        });
      }else{
        if(fb_id=="0"){
          res.send(
               {result: false, msg: "Admin Already Exists" }
            );
        }else{
          res.send(
               {result: true, data: docs }
            );
        }
      }
    });
  })

  .get('/userlogin/:email/:pwd', function(req, res) {
    var email = req.params.email;
    var pwd = sha1(req.params.pwd); //req.params.pwd;
    User.find({'email':email, 'pwd':pwd},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .get('/userlist', function(req, res) {
    User.find({},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })


  //signup/kapil karda/kkarda77@gmail.com/kapilkk/male/0/0/1/IN/30-03-1993/image
  .get('/signup/:fullname/:email/:pwd/:gender/:fb_id/:fb_token/:userstatus/:country/:dob/:avator', function(req, res) {
    
    var fullname = req.params.fullname;
    var email = req.params.email;
    var pwd = sha1(req.params.pwd);
    var gender = req.params.gender;
    var fb_id = req.params.fb_id;
    var fb_token = req.params.fb_token;
    var userstatus = req.params.userstatus;
    var country = req.params.country;
    var dob = req.params.dob;
    var avator = req.params.avator;

    User.find({'email':email},{},function(e,docs){
      if(docs.length==0){

        if(fb_id!="0"){
          avator = "https://graph.facebook.com/"+fb_id+"/picture";
        }else{
          avator = "https://betterbrain.herokuapp.com/uploadimage/default_user.jpg";
        }

        var user = new User({'fullname':fullname, 'username':fullname, 'email':email, 'pwd':pwd, 'gender':gender, 'fb_id':fb_id, 'fb_token':fb_token, 'userstatus':userstatus, 'avator':avator, 'country':country, 'dob':dob});
        user.id = user._id;
        user.save(function (err) {
          //res.json(200, todo);
          var user_point = new Userpoint({ 'user_id':user._id, 'question_attempt':0, 'right_question':0, 'wrong_question':0, 'total_point':0 });
          user_point.id = user_point._id; 
          user_point.save(function (err1){
            res.send(
                (err === null) ? { result: true, data: user  } : {result: false, msg: err }
            );
          });
        });
      }else{
        if(fb_id=="0"){
          res.send(
               {result: false, msg: "User Already Exists" }
            );
        }else{
          res.send(
               {result: true, data: docs }
            );
        }
      }
    });
  })

  .get('/updateusername/:user_id/:username', function(req, res) {
    var user_id  = req.params.user_id;
    var username = req.params.username;
    User.update({_id:user_id}, {$set: {"username":username}},function(e, docs){
        res.send(
          (e === null) ? { result: true, msg: "updated"  } : {result: false, msg: e }
        );
    });
  })

  .get('/topics', function(req, res) {
    Topic.find({},{},function(e,docs){
        if(docs.length!=0){
            res.send({ result: true, data: docs });
        }else{
            res.send({ result: false, data: "No Topic Found"});    
        }
    });
  })

  .get('/question_type', function(req, res) {
    QuestionType.find({},{},function(e,docs){
        if(docs.length!=0){
            res.send({ result: true, data: docs });
        }else{
            res.send({ result: false, data: "No Type Found"});    
        }
    });
  })

  .get('/getQuestions/:topic_id', function(req, res) { 
    // Question.findRandom().limit(10).exec(function (err, docs) {
    //   if (!err) {
    //     res.send({ result: true, data: docs });
    //   }else{
    //     res.send({ result: false, data: "No Question Found"});
    //   }
    // });
      
    var topic_id = req.params.topic_id;
    Question.findRandom({}, {}, {limit : 10}, function(err, results) {
      if (!err) {
        res.send({ result: true, data: results });
      }else{
        res.send({ result: false, data: "No Question Found"});
      }
    });
  })

  .get('/questions', function(req, res) { 
    Question.find({},{},function(e,docs){
        if(docs.length!=0){
            res.send({ result: true, data: docs });
        }else{
            res.send({ result: false, data: "No Question Found"});    
        }
    });
  })

  .get('/setQuestionType/:title', function(req, res) {
    var title = req.params.title;
    QuestionType.find({'title':title},{},function(e,docs){
        if(docs.length==0){
            var question_type = new QuestionType({'title':title, 'question_coun':0});
            question_type.id = question_type._id;            
            question_type.save(function (err) {
                res.send(
                    (err === null) ? { result: true, data: question_type  } : {result: false, msg: err }
                );
            });

        }else{
            res.send(
               {result: true, data: "Type is Already Exists" }
            );
        }
    });
  })

  .get('/setTopics/:title/:image', function(req, res) {
    var title = req.params.title;
    var image = req.params.image;
    Topic.find({'title':title},{},function(e,docs){
        if(docs.length==0){
            var topic = new Topic({'title':title, 'icon':image, 'question_coun':0});
            topic.id = topic._id;            
            topic.save(function (err) {
                res.send(
                    (err === null) ? { result: true, data: topic  } : {result: false, msg: err }
                );
            });
        }else{
            res.send(
               {result: true, data: "Topic is Already Exists" }
            );
        }
    });
  })


  .post('/setQuestion', function(req, res) {
   
    var question = req.body.question;
    var type_id = req.body.type_id;
    var topic_id = req.body.topic_id;
    var opt1 = req.body.opt1;
    var opt2 = req.body.opt2;
    var opt3 = req.body.opt3;
    var opt4 = req.body.opt4;
    var right_answer = req.body.right_answer;
    var category = req.body.category;
    var sub_category = req.body.sub_category;
    var level = req.body.level;
    var type = req.body.type;

    var stacksizebb = req.body.stacksizebb;
    var bb = req.body.bb;
    var sb = req.body.sb;
    var ante = req.body.ante;
    var card1 = req.body.card1;
    var card2 = req.body.card2;
    var position = req.body.position;

    //request.body.user.name

    var question = new Question({'question':question, 'type_id':type_id, 'topic_id':topic_id, 'opt1':opt1, 'opt2':opt2, 'opt3':opt3, 'opt4':opt4, 'right_answer':right_answer, 'category':category, 'sub_category':sub_category, 'level':level, 'type':type, 'stacksizebb':stacksizebb, 'bb':bb, 'sb':sb, 'ante':ante, 'card1':card1, 'card2':card2, 'position':position});
    question.id = question._id;            
    question.save(function (err) {

        Topic.findOne({_id:topic_id}, {}, function(e, user){
          var question_coun = Number(user.question_coun)+Number(1);
          Topic.update({_id:topic_id}, {$set: {"question_coun":question_coun}},function(e, docs){
              res.send(
                  (err === null) ? { result: true, data: question  } : {result: false, msg: err }
              );
          });
        });
    });
  })


  .get('/insterScore/:topic_id/:user_id/:question_id/:answer/:point/:isTrue/:isFalse', function(req, res) {
    
    var topic_id = req.params.topic_id;
    var user_id = req.params.user_id;
    var question_id = req.params.question_id;
    var answer = req.params.answer;
    var point = req.params.point;
    var isTrue = req.params.isTrue;
    var isFalse = req.params.isFalse;

    // InsertScore.find({'user_id':user_id, 'question_id':question_id},{},function(e,docs){
    //     if(docs.length==0){
          var insert_score = new InsertScore({'topic_id':topic_id, 'user_id':user_id, 'question_id':question_id, 'answer':answer, 'point':point, 'isTrue':isTrue, 'isFalse':isFalse});
          insert_score.id = insert_score._id;            
          insert_score.save(function (err) {
              Userpoint.findOne({user_id:user_id}, {}, function(e, user){
                var question_attempt = Number(user.question_attempt)+1;
                if(isTrue=="1" || isTrue==1){
                  var right_question = Number(user.right_question)+1;
                  var wrong_question = Number(user.wrong_question);
                }else{
                  var right_question = Number(user.right_question);
                  var wrong_question = Number(user.wrong_question)+1;
                }                 
                var total_point = Number(user.total_point)+Number(point);
                Userpoint.update({user_id:user_id}, {$set: {"question_attempt":question_attempt, "right_question":right_question, "wrong_question":wrong_question, "total_point":total_point}},function(e, docs){
                    res.send(
                      (err === null) ? { result: true, data: insert_score  } : {result: false, msg: err }
                    );
                });
              });
          });
    //     }else{
    //       res.send(
    //           {result: false, msg: "Already Added" }
    //       );
    //     }
    // });
  })

  .get('/getScores/:user_id', function(req, res) {

    var user_id = req.params.user_id;
    InsertScore.find({'user_id':user_id},{}).populate('user_id').populate('question_id')
      .exec(function(err, projects){

          res.send({"data":projects});

      });
  })


  .get('/removeQuestion/:question_id', function(req, res) {
    var question_id = req.params.question_id;
    Question.remove({_id:question_id},function(e,docs){
        if(e===null){
            res.send({"error":false, "message":"Successfully Deleted."});
        }else{
            res.send({"error":true, "message":"Opps! Something went wrong."});
        }   
    });
  })

  .get('/removeUser/:user_id', function(req, res) {
    var user_id = req.params.user_id;
    User.remove({_id:user_id},function(e,docs){
        if(e===null){
            res.send({"error":false, "message":"Successfully Deleted."});
        }else{
            res.send({"error":true, "message":"Opps! Something went wrong."});
        }   
    });
  })

  .get('/leaderBoard', function(req, res) {
    Userpoint.find({ user_id: { $ne: null } }).limit(100).sort( { total_point: -1 } ).populate('user_id')
      .exec(function(err, projects){
          res.send({"data":projects});
      });
  })

  .get('/updatePoint/:user_id', function(req, res) {
    var user_id = req.params.user_id;
    Userpoint.findOne({user_id:user_id}, {}, function(e, user){
        var question_attempt = Number(user.question_attempt)+1;
        var right_question = Number(user.right_question)+1;
        var wrong_question = Number(user.wrong_question)+1;
        var total_point = Number(user.total_point)+1;
        Userpoint.update({user_id:user_id}, {$set: {"question_attempt":question_attempt, "right_question":right_question, "wrong_question":wrong_question, "total_point":total_point}},function(e, docs){
          res.send(docs);
        });
    });
  })

  .post('/uploadImagePath/:user_id', function(req, res) {
    var image = req.body.image;
    var user_id = req.params.user_id;
    User.update({_id:user_id}, {$set: {"avator":image}},function(e, docs){
        res.send(
          (e === null) ? { result: true, msg: "updated"  } : {result: false, msg: e }
        );
    });
  })

  .post('/uploadImagePath1/:user_id', function(req, res) {
    var filename = req.file;
    var user_id = req.params.user_id;
    upload(req,res,function(err, data) {
      if(err) {
        return res.end("Error uploading file.");
      }else{
        var image = req.files.file.path;
        var newimage  = "https://betterbrain.herokuapp.com/"+image;
        cloudinary.uploader.upload(newimage, function(result) { 
          var mainImage = result.secure_url;
          User.update({_id:user_id}, {$set: {"avator":mainImage}},function(e, docs){
            res.send(
                (e === null) ? { result: true, msg: "updated", imagepath: mainImage  } : {result: false, msg: e }
            );
          });
        });
      }
    });
  })

  .get('/removeTopic/:topic_id', function(req, res) {
    var topic_id = req.params.topic_id;
    Topic.remove({_id:topic_id},function(e,docs){
        if(e===null){
            res.send({"error":false, "message":"Successfully Deleted."});
        }else{
            res.send({"error":true, "message":"Opps! Something went wrong."});
        }   
    });
  })

  //Userpoint InsertScore
  .get('/removePoints', function(req, res) {
    Userpoint.remove({},function(e,docs){
        if(e===null){
            InsertScore.remove({},function(e,docs){
              if(e===null){
                  res.send({"error":false, "message":"Successfully Deleted."});
              }else{
                  res.send({"error":true, "message":"Opps! Something went wrong."});
              }   
          });
        }else{
            res.send({"error":true, "message":"Opps! Something went wrong."});
        }   
    });
  })

  .get('/removenull/:_id', function(req, res) {
    var _id = req.params._id;
    Userpoint.remove({ _id: _id },function(e,docs){
        if(e===null){
            res.send({"error":false, "message":"Successfully Deleted."}); 
        }else{
            res.send({"error":true, "message":"Opps! Something went wrong."});
        }   
    });
  })

  .get('/removeUsers', function(req, res) {
    User.remove({},function(e,docs){
        if(e===null){
            res.send({"error":false, "message":"Successfully Deleted."}); 
        }else{
            res.send({"error":true, "message":"Opps! Something went wrong."});
        }   
    });
  })

  .get('/uploadSpread', function(req, res) {
   
    var my_sheet = new GoogleSpreadsheet('1VncyEMW2NuYGcVvaRtaCass7KBErLJlKQux2buf3E0s');
      my_sheet.useServiceAccountAuth(creds, function(err){
        my_sheet.getInfo( function( err, sheet_info ){
            var sheet1 = sheet_info.worksheets[0];
            res.send({"error":err,"rows":sheet1.rowcounts});
            // sheet1.getRows( function( err, rows ){
            //   res.send({"error":err,"rows":rows});
              //res.jsonp(rows);
            //});
        });

    })
  })

  .use(express.static(__dirname + '/'))
  .listen(process.env.PORT || 5000);
